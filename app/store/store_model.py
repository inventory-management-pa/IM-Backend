from sqlalchemy.sql import functions
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import DateTime, Integer, String
from app.database.dbconfig import Base
from sqlalchemy import Column


class Store(Base):
    __tablename__ = 'store'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    phone = Column(String)
    user_id = Column(Integer, ForeignKey('user.id'))
    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(DateTime,
                        onupdate=functions.now())
    deleted_at = Column(DateTime,
                        onupdate=functions.now())
