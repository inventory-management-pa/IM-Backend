import typing
from fastapi.responses import JSONResponse
from starlette.background import BackgroundTask


class CustomResponse(JSONResponse):

    def __init__(
        self,
        content:
        typing.Any = None,
            status_code: int = 200,
            headers: dict = None,
            media_type: str = None,
            background: BackgroundTask = None) -> None:

        message = ''

        data = {}

        if isinstance(content, dict):
            message = content.pop('message', message)
            data = content.pop('payload', data)
            res = {
                'message': message,
                'payload': data
            }
        else:
            res = {
                'payload': content
            }

        super().__init__(content=res, status_code=status_code,
                         headers=headers, media_type=media_type, background=background)
