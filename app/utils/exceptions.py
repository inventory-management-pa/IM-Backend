class GeneralException(Exception):
    def __init__(self, message: str):
        self.message = message


class ApiErrorTypes:
    GeneralError = 'GeneralError'
    SystemError = 'SystemError'


class ApiErrors:
    ProductNotFound = 'Product not found'
