from sqlalchemy.sql.sqltypes import Integer, String
from app.database.dbconfig import Base
from sqlalchemy import Column


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    password = Column(String)
