from app.utils.hash import *
from sqlalchemy.orm import Session

from . import schemas
from . import user_model


def get_user(db: Session, user_id: int):
    return db.query(user_model.User).filter(user_model.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(user_model.User).filter(user_model.User.email == email).first()

# def get_users(db: Session, skip: int = 0, limit: int = 100):
#     return db.query(user_model.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserLogin):
    hashed_password = get_password_hash(user.password)
    db_user = user_model.User(email=user.email, password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def authenticate_user(email: str, password: str, db):
    user = get_user_by_email(db, email)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user
