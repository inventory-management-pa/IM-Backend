
from fastapi import FastAPI, Request, status
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.user import user_controller
from app.utils.exceptions import ApiErrorTypes, GeneralException
from app.utils.response import CustomResponse

from .database.dbconfig import engine, Base

app = FastAPI(default_response_class=CustomResponse)

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(user_controller.router, prefix='/users')

Base.metadata.create_all(bind=engine)


# Exception Handlers.
@app.exception_handler(GeneralException)
async def general_exception_handler(request: Request, exc: GeneralException):
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={'message': exc.message, 'type': ApiErrorTypes.GeneralError}
    )


@app.get("/")
async def root():
    return "Hello World"
