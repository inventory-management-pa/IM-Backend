
from sqlalchemy import Column
from sqlalchemy.sql import functions
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import DateTime, Integer, String
from app.database.dbconfig import Base


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    specifications = relationship("specification")
    category_id = Column(Integer, ForeignKey('category.id'))
    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(DateTime,
                        onupdate=functions.now())
    deleted_at = Column(DateTime,
                        onupdate=functions.now())


class Specification(Base):
    __tablename__ = 'specification'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    product_id = Column(Integer, ForeignKey('product.id'))
    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(DateTime,
                        onupdate=functions.now())
    deleted_at = Column(DateTime,
                        onupdate=functions.now())


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    count = Column(Integer)
    date_in = Column(DateTime)
    store_id = Column(Integer, ForeignKey('store.id'))
    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(DateTime,
                        onupdate=functions.now())
    deleted_at = Column(DateTime,
                        onupdate=functions.now())
