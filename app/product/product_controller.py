from fastapi import APIRouter
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session

from app.database.dbconfig import get_db
from app.product.product_dto import AddProductDTO, ProductDTO

from . import product_repository

router = APIRouter(tags=['products'])


@router.get('/{category_id}')
def get_products(category_id: int, db: Session = Depends(get_db)):
    return product_repository.get_products(category_id=category_id, db=db)


@router.post('/{category_id}', response_model=ProductDTO)
def add_product(category_id: int, add_product_dto: AddProductDTO, db: Session = Depends(get_db)):
    return product_repository.add_product(category_id, add_product_dto, db)
