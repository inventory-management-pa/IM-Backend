from sqlalchemy.orm.session import Session
from datetime import date
from app.database.helpers import save
from app.product.product_dto import AddProductDTO
from . import product_model


def get_products(category_id, db: Session):
    return db.query(product_model.Product).filter(category_id=category_id).all()


def add_product(add_product_dto: AddProductDTO, db: Session):
    product = product_model.Product(name=add_product_dto.name,
                                    count=add_product_dto.count, date_in=date.today())
    return save(db, product)
